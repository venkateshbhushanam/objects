

function mapObject(obj, cb) {
  
    let mapObj={}
    if (cb===null || cb===undefined || cb.length===0){
        return {}
    }

    if(obj==={}){
        return {}
    }

    for (let eachItem in obj){

        mapObj[eachItem]=(obj[eachItem])+cb
       
    }
    return mapObj
}

module.exports=mapObject