function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties 
    //on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    //console.log(obj,defaultProps)
    let newObj={...obj}

    if(typeof(defaultProps)==="string" || typeof(defaultProps)==="number" || defaultProps==={}){
        return {}
    }
    
    for (let each in obj){

        for (let item in defaultProps){
            if (item===each){
                continue
            }
            else{
                newObj[item]=defaultProps[item]
                break

            }
        }



    }
  return newObj
}

module.exports=defaults